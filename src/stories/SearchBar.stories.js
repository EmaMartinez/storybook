import React from 'react';
import SearchBar from './SearchBar'


export default {
  title: 'Example/SearchBar',
  component: SearchBar,
  argTypes: {
  },
};

const Template = (args) => <SearchBar {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    label: 'Search...',
};
