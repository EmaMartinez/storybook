
import React from 'react';

import Button from './Button';
import SearchBar from './SearchBar'
import './header.css'

export const Header = ({ theme }) => (
  <header>
  <div>
    {theme ? (
      <>
        <div className="wrapperL">
          <div>
            <Button label="Light" />
            <Button label="Light" />
            <Button label="Light" placeholder="Light" />
          </div>
          <div>
            <SearchBar />
          </div>
        </div>
      </>
    ) : (
        <>
          <div className="wrapperD">
            <div>
              <Button label="Dark" />
              <Button label="Dark" />
              <Button label="Dark" />
            </div>
            <div>
              <SearchBar />
            </div>
          </div>
        </>
      )}
  </div>
  </header>
);



