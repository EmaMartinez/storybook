import React, { useState } from 'react';

const SearchBar = ({ label }) => {
    const [search, setInput] = useState("");

    const handleSubmit = (evt) => {
        evt.preventDefault();
        alert(`Submitting search ${search}`)
    }
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <input
                        sx={{

                        }}
                        type="text"
                        value={search}
                        onChange={e => setInput(e.target.value)}
                        placeholder={label} />
                </div>
            </form>
        </div>
    );

}

export default SearchBar;
