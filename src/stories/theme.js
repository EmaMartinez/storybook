export default
    {
        colors: {
            text: '#000',
            background: '#fff',
            primary: 'white',
            secondary: '#1ea7fd',
        },
        fonts: {
            body: 'Roboto, sans-serif',
            heading: 'Georgia, serif',
        },
        fontWeights: {
            body: 700,
            heading: 600,
        },
        styles: {
            root: {
                fontFamily: 'body',
                fontWeight: 'body',
            },
        },
        buttons: {
            borderRadius: '3em',
            cursor: 'pointer',
            display: 'inline-block',
            lineHeight: 1,
            fontSize: '14px',
            padding: '11px 20px',
          },
    }