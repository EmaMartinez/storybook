import React from 'react';

import { Header } from './Header';

export default {
  title: 'Example/Header',
  component: Header,
};

const Template = (args) => <Header {...args} />;

export const Light = Template.bind({});
Light.args = {
  theme: {Light},
};

export const Dark = Template.bind({});
Dark.args = {
};
