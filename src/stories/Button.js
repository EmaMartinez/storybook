/** @jsxRuntime classic */
/** @jsx jsx */
import {jsx} from 'theme-ui';

const Button = ({primary, backgroundColor, size, label, ...props}) => (
  <button
      type="button"
      style={backgroundColor && { backgroundColor }}
      {...props}
    sx={{
      fontWeight: 'body',
      border: 0,
      color: 'primary',
      backgroundColor: 'secondary',
      borderRadius: '3em',
      cursor: 'pointer',
      display: 'inline-block',
      lineHeight: 1,
      fontSize: '14px',
      padding: '11px 20px',
      
    }}>
      {label}
  </button>
);

export default Button;
