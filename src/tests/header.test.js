import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Light } from '../stories/Header.stories'


it('renders the header in the primary state', () => {
    render(<Light {...Light.args} />);
    expect(screen.getByPlaceholderText('Light')).toHaveTextContent('Light');
  });
  