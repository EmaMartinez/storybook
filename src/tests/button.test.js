import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Primary } from '../stories/Button.stories';

it('renders the button in the primary state', () => {
    render(<Primary {...Primary.args} />);
    expect(screen.getByRole('button')).toHaveTextContent('Button');
});