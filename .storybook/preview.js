import {addDecorator} from '@storybook/react';

/** @jsxRuntime classic */
/** @jsx jsx */
import {jsx, ThemeProvider} from 'theme-ui';
import theme from '../src/stories/theme';

addDecorator(storyFn => (
  <ThemeProvider theme={theme}>
    {storyFn()}
  </ThemeProvider>
));
